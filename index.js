// function rowSumOddNumbers(n) {
//   var start = n * n - n + 1;
//   var result = 0;

//   for (i = 0; i < n; i++) {
//     result = result + (start + i * 2);
//   }

//   return result;
// }

// console.log(rowSumOddNumbers(7));

// function rowSumOddNumbers(n) {
//   // TODO
//   return n > 0 ? n * n * n : "Wrong Input";
// }
// console.log(rowSumOddNumbers("apple"));
// //

// function narcissistic(value) {
//   const value1 = value;
//   let sum = 0;
//   while (value > 0) {
//     rem = value % 10;
//     sum += rem * rem * rem;
//     value = Math.floor(value / 10);
//   }
//   return sum === value1 ? true : false;
// }
// console.log(narcissistic(371));

// const sing = function (lyrics) {
//   let arr = [];
//   lyrics = lyrics.split(".");

// console.log(lyrics);
// forEach((word, i) => {
//   let a = 0;
//   let b;
//   if (lyrics.indexOf(".")) {
//     b = i;
//     arr.push(lyrics.substring(a, b));
//     a = b;
//   }
// });

// return arr;
// };

// let lyrics = `99 bottles of beer on the wall, 99 bottles of beer.
// Take one down and pass it around, 98 bottles of beer on the wall.

// 98 bottles of beer on the wall, 98 bottles of beer.
// Take one down and pass it around, 97 bottles of beer on the wall.`;
// // let songs = lyrics;

// // songs = lyrics.split("\n");
// // song1 = songs.filter((song) => {
// //   if (song.length > 0) return true;
// // });
// // console.log(song1);
// const sing = function (lyrics) {
//   let songs = [];
//   songs = lyrics.split("\n");
//   songs1 = songs.filter((song) => {
//     if (song.length > 0) return true;
//   });
//   return songs1;
// };
// console.log(sing(lyrics));
// let b,
//   c = [];
// const arr = [2, 3, 4, null, 4, "", 5, []];
// b = arr.map((el) => typeof el);
// console.log(arr);
// console.log(b);

// c = arr.filter((el) => {
//   if (el === null || el === "" || el === []) return false;
// });
// console.log(b);

///// Data types

// let x = 16 + "nepal";
// while adding number to a string JavaScript will treat number as string

///// detectWord 0
// function detectWord(str) {
//   let word = "";
//   for (let i = 0; i < str.length; i++) {
//     if (str[i] !== str[i].toUpperCase()) word += str[i];
//   }
//   return word;
// }
// console.log(detectWord("UcUNFYGaFYFYGtNUH"));

// solved
// const detectWord = (str) => str.replace(/[A-Z]/g, "");
// console.log(detectWord("UcUNFYGaFYFYGtNUH"));

// ///// user will input error number and we will display error message
// function error(n) {
//   if (n > 6) return "101";
//   error = [
//     "Check the fan",
//     "Emergency stop",
//     "Pump Error",
//     "c",
//     "Temperature Sensor Error",
//   ];
//   return `${error[n - 1] + " " + "e" + n}`;
// }
// console.log(error(3));

// // solved
// function error(n) {
//   return (
//     {
//       1: "Check the fan: e1",
//       2: "Emergency stop: e2",
//       3: "Pump Error: e3",
//       4: "c: e4",
//       5: "Temperature Sensor Error: e5",
//     }[n] || 101
//   );
// }

// ///// can one array nest inside another

// function canNest(arr1, arr2) {
//   const max1 = Math.max(...arr1);
//   const max2 = Math.max(...arr2);
//   const min1 = Math.min(...arr1);
//   const min2 = Math.min(...arr2);
//   return min1 > min2 && max1 < max2 ? true : false;
// }
// console.log(canNest([1, 2, 3, 4], [0, 6]));

// //// mimicing left shift operator
// // left shift operator: a << b
// function shiftToLeft(x, y) {
//   return x * Math.pow(2, y);
// }
// console.log(shiftToLeft(10, 3));

// ///// which function is larger
// // ternary operator can be chained
// function whichIsLarger(f, g) {
//   return f() > g() ? "f" : g() > f() ? "g" : "neither";
// }
// console.log(
//   whichIsLarger(
//     () => 10,
//     () => 10
//   )
// );

// ///// Building a simple promise
// let promise = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     resolve("subodh");
//   }, 1000);
// });
// promise.then((res) => console.log(res)).catch((err) => console.log(err));

// ///// convert base 10 number to binary number
// function binary(decimal) {
//   return decimal.toString(2);
// }
// console.log(binary(5));

// ///// create a class and compare ages
// class Person {
//   constructor(name, age) {
//     this.name = name;
//     this.age = age;
//   }
//   compareAge(p) {
//     return `${p.name} is ${
//       p.age > this.age
//         ? "older than"
//         : p.age < this.age
//         ? "younger than"
//         : "same age as"
//     } me`;
//   }
// }
// const p1 = new Person("Samuel", 24);
// const p2 = new Person("Joel", 36);
// const p3 = new Person("Lily", 24);

// console.log(p1.compareAge(p3));

// ///// Bitwise operations
// function bitwiseAND(n1, n2) {
//   n1 = n1.toString(2).padStart(8, "0");
//   n2 = n2.toString(2).padStart(8, "0");
//   let o = "";
//   for (let i = 0; i < n1.length; i++) {
//     o += n1[i] === "1" && n2[i] === "1" ? "1" : "0";
//   }
//   return o;
// }

// function bitwiseOR(n1, n2) {
//   n1 = n1.toString(2).padStart(8, "0");
//   n2 = n2.toString(2).padStart(8, "0");
//   let o = "";
//   for (let i = 0; i < n1.length; i++) {
//     o += n1[i] === "1" || n2[i] === "1" ? "1" : "0";
//   }
//   return o;
// }

// function bitwiseXOR(n1, n2) {
//   n1 = n1.toString(2).padStart(8, "0");
//   n2 = n2.toString(2).padStart(8, "0");
//   let o = "";
//   for (let i = 0; i < n1.length; i++) {
//     o += n1[i] === n2[i] ? "0" : "1";
//   }
//   return parseInt(o, 2).toString(10);
// }
// console.log(bitwiseAND(6, 23));
// console.log(bitwiseOR(6, 23));
// console.log(bitwiseXOR(6, 23));
// console.log(parseInt(bitwiseAND(7, 12), 2).toString(10));

// ///// Check if its Christmas or not
// function timeForMilkAndCookies(date) {
//   const month = date.getMonth();
//   const day = date.getDate(); // getDay()  Sun, Mon
//   console.log(month, day);
//   return month === 11 && day === 24 ? true : false;
// }

// console.log(timeForMilkAndCookies(new Date(2013, 11, 24)));

// ///// sorting drinks by price
// drinks = [
//   { name: "lemonade", price: 50 },
//   { name: "lime", price: 10 },
// ];
// function sortDrinkByPrice(drinks) {}

// sortDrinkByPrice(drinks);
// const a = undefined;
// a = 5;

// const sum = function (a, b) {
//   const result = () => a + b;
//   return result(a, b);
// };
// console.log(sum(5, 6));

// function a() {
//   return "a is function statement or declaration";
// }

// const b = function () {
//   return "b is function expression";
// };

// const c = function c1() {
//   return "c is named function expression";
// };

// const d = function () {
//   return function d1() {
//     return "hello";
//   };
// };

// console.log(d());
// console.log(d()());

// // hoisting is the behaviour of moving declarations to the top

// ///// checks whether a number is perfect square or not
// var isSquare = function (n) {
//   const a = Math.pow(n, 1 / 2);
//   return a === Math.round(a) ? true : false;
// };
// console.log(isSquare(25));
// console.log(isSquare(22));

// ///// get middle character from the string
// function getMiddle(s) {
//   const start = Math.trunc(s.length / 2);
//   return s.length % 2 === 0
//     ? s.slice(start - 1, start + 1)
//     : s.slice(start, start + 1);
// }
// console.log(getMiddle("555555"));
// function multiply(n) {
//   let m = 1;
//   while (n > 0) {
//     let r = n % 10;
//     m *= r;
//     n = Math.floor(n / 10);
//   }
//   return m;
// }

// function separate1multiply(n) {
//   const output = [],
//     sNumber = n.toString();

//   for (var i = 0, len = sNumber.length; i < len; i += 1) {
//     output.push(+sNumber.charAt(i));
//   }
//   return output.reduce((acc, el) => acc * el);
// }
// // console.log(separate1multiply(134));

// function persistance(num) {
//   for (let i = 1; i < 10; i++) {
//     let r = separate1multiply(num);
//     console.log(r);
//     if (r < 10) {
//       return i;
//     }
//     num = r;
//   }
// }
// console.log(persistance(77));

///// array methods

// some()

// const result1 = names.some((name) => {
//   return name === "hari";
// });
// console.log(result1);
// const names = ["ram", "shyam", "hari", "ganesh"];
// const checkName1 = function (name1) {
//   console.log(name1);
//   return (checked = names.some((el) => el === name1));
// };
// console.log(checkName1("hari"));

// // every()
// const ages = [34, 67, 99, 34, 55, 12];
// const a = ages.every((el) => el > 18);                                                                          [[[[[[[[[[[[[[[[[[[[[[[[[[                                                                                   ''' '.iiiiiiiock6ikkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk]]]]]]]]]]]]]]]]]]]]                  [[[[]]]]
// console.log(a);

///// reduce()
// arr = [3, 2, 4, 1];
// console.log(arr.reduce((sum, el) => sum + el, 10));

// function getMultiply(total, el) {
//   return total * el;
// }
// console.log(arr.reduce(getMultiply, 1));

///// map()

// bill = [20, 50, 40];
// let finalBill = [];
// finalBill = bill.map((el) => el + 0.15 * el + 5);
// console.log(finalBill);

// ages = [12, 34, 8, 99];
// let ageDescription = [];
// ageDescription = ages.map((el) =>
//   el < 18 ? "young" : el > 50 ? "old" : "medium"
// );
// console.log(ageDescription);

///// flat(), array.flat(depth)
// flat returns a new array with the sub-array elements concatednated into it
// flat also removes empty slots in array

// const arr = [2, , , 7, 4, [4, 6, [5, 6]]];
// arrNewf1 = arr.flat();
// arrNewf2 = arr.flat(2);
// arrNewf3 = arr.flat(3);
// console.log(arrNewf1);
// console.log(arrNewf2);
// console.log(arrNewf3);
// console.log(arr);

///// fill(), array.fill(value,start,end)
// mutates the original array
// let arr = [33, 56, 77, 55];

// newArr2 = arr.fill("disqualified", 1, 2);

// console.log(newArr2);
// console.log(arr);

///// includes() determines whether an arrary includes certain value among its entries returing true or false
// array.includes(searchElement, fromIndex)
// let arr = [33, 56, 77, 55];
// console.log(arr.includes(55));
// console.log(arr.includes(59));
// console.log(arr.includes(59, 3));
// const a = "apple";
// console.log(a.includes("p"));
// console.log(a.includes("j"));

///// findIndex()
// returns the index of the first array element that passes the test
// if none fount returns -1
// let arr = [33, 56, 77, 55, 5];
// const i = arr.findIndex((el) => el === 77);
// const j = arr.findIndex((el) => el < 10);
// const k = arr.findIndex((el) => el > 100);
// console.log(i);
// console.log(j);
// console.log(k);

///// reverse()
// reverse method reverses the order of the elements in an array
// and overwrites the original array
// const arr = [33, 56, 77, 55];
// arr.reverse();
// console.log(arr);

///// flatMap()

// ages = [12, 34, 8, 99];
// let ageDescription = [];
// ageDescription = ages.flatMap((el) => {
//   let des = el < 18 ? "young" : el > 50 ? "old" : "medium";
//   return [el, des];
// });
// console.log(ageDescription);
// console.log(ages);

// generate list of words from lists of sentences

// arrStr = ["I am Happy TOday", "I will go Bouddha tomorrow"];
// let arrStr1 = arrStr.flatMap((el) => el.split(" "));
// console.log(arrStr1);

// remove negative numbers, even number no chage, odd number = even +1
// let a = [3, 4, -7, 3, -8, 12];
// let b = a.flatMap((el) => (el < 0 ? [] : el % 2 === 0 ? [el] : [el - 1, 1]));
// console.log(b);

///// split() string.split(seperator)
// splits a string into an array of substrings, and returns the new array
// seperate each words from the given sentence
// let words = [];
// let sentence = "I am going to Pokhara tomorrow";
// words = sentence.split(" ");
// console.log(words);

// let numArr = [];
// let numberStr = "12345";
// numArr = numberStr.split("");
// console.log(numArr);
// numArr1 = numArr.map((el) => +el);
// console.log(numArr1);

///// indexOf() returns the position of the first occurance of a specified value in a string
// returns -1 if the value is not found

// const arr = [56, 89, 77, 23, 43, 95];
// let i = arr.indexOf(Math.max(...arr));
// console.log(i);
// const newArray = arr.filter((el) => el > 60);
// console.log(newArray);

///// forEach() method calls a function once for each element in an array, in order
// arr = [3, 4, 5];
// let b = [];
// arr.forEach(function (el) {
//   b.push(el + 10);
// });
// console.log(b);

///// find() returns the first value of the array element that passes the test

arr = ["ram", "shyam", "hari", "ganesh"];
const name1 = arr.find((el) => el.startsWith("g"));
console.log(name1);

const inventories = [{
  {name:"apples",quantities:4},
  {name:"mango",quantities:9},
  {name:"orange",quantities:6},
}];
